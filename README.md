# Grid5000 Cluster scripts

These cluster scripts are used for accessing the grid500 clusters if you have access to it. These clusters are scheduled with [OAR](http://oar.imag.fr/). 

**g5k book** : for booking a node

	zaza:$ g5k book -h
	Usage :
	Books a node on the grid5000 cluster 

	   -u, --user <login>          login to connect to grid5000
	   -m, --machine <machine>     OPTIONAL, a specific machine
	   -s, --site <site>  	       the site (default: nancy)
	   -c, --cluster <cluster>     the cluster (default: gros)
 	   -w, --walltime <walltime>   in hours (default: 2)
	   -h, --help                  prints this help message

**g5k log** : for logging to a booked node
					
	zaza:$ g5k log -h	
	Usage: 
	Logs to an already booked node on the grid5000 cluster

 	   -u, --user <login>          login to connect to grid5000
   	   -f, --site <SITE>           Site (default: nancy)
   	   -j, --jobid <JOB_ID>        The JOB_ID to which to connect. If not provided
               	                       a list of your booked JOB_ID will be displayed
   	   -h, --help                  prints this help message


**g5k kill** : for releasing a booked node

	zaza:$ g5k kill -h
	Usage :
	Deletes a reservation on the grid5000 cluster

	   -u, --user <login>          Login to connect to grid5000
	   -s, --site <SITE>           the site (default: nancy)
	   -j, --jobid <JOB_ID>        The JOB_ID to delete. If not provided
                                       a list of your booked JOB_ID will be displayed
	   -j, --jobid all             Will kill all the jobs booked by <login>
	   -h, --help                  Prints this help message


**g5k port_forward**: for forwarding a port from a booked node to your localhost

	zaza:$ g5k port_forward -h
	Usage :
	Forward a port from a machine you booked to your local computer

	   -u, --user <login>          login to connect to grid5000
	   -j, --jobid <JOB_ID>        The JOB_ID to which to connect. If not provided a list of your booked JOB_ID will be displayed
	   -m, --machine <MACHINE>     The booked hostname, optional
	   -p, --port <PORT>           The distant port <PORT> will be binded to localhost:PORT
	   -h, --help                  prints this help message 



# Usage example (jupyter notebook)

### Booking a node :

<img src="https://gitlab.inria.fr/jolegran/g5k-scripts/-/raw/master/images/book.png" height="150" title="booking a node">



### Login to the booked node :

<img src="https://gitlab.inria.fr/jolegran/g5k-scripts/-/raw/master/images/log.png" height="110" title="login to a node">



### Installing and starting jupyter notebook on the booked node :

Jupyter can be installed on grid5000 locally using the command (need to be done once) :

	pip install jupyterlab

Then, you can start jupyter notebook : 

![starting jupyter on the node](/images/jupyter.png)



### Forwarding the port :

<img src="https://gitlab.inria.fr/jolegran/g5k-scripts/-/raw/master/images/port_forward.png" height="100" title="forwarding port">




### Starting jupyter locally :

You can now start jupyter locally using the provided address.

![local jupyter](/images/jupyter_local.png)
